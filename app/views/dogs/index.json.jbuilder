json.array!(@dogs) do |dog|
  json.extract! dog, :id, :name, :colour
  json.url dog_url(dog, format: :json)
end
