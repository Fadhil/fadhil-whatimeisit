class UserMailer < ActionMailer::Base
  if Rails.env == :production
    
    default from: "noreply@whatimeisit-fadhil2.herokuapp.com" 
  else
    
    default from: "noreply@fadhil-whatimeisit-c9-fadhil1.c9.io"
  end
  
  def welcome_email(user)
      @user = user
      @url = sign_in_url
      
      mail( to: user.email, subject: 'Thanks for registering')
  end
end


