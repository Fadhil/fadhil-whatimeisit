class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
    validates :name, presence: true
    validates :email,   presence: true, 
                        format: { with: /\A\S+@\w+\.\w+\z/, 
                            message: 'format is invalid' }
                            

    #has_secure_password
end
