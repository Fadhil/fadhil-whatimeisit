class SessionsController < ApplicationController
    def new
        
    end
    
    def create
        @user = User.where(email: params[:session][:email]).first
        
        if @user
            if @user.authenticate(params[:session][:password]) 
               session[:user_id] = @user.id
               redirect_to root_path, notice: 'Successfully Signed In'
            else
                flash[:notice] = "Incorrect Username/Password"
                render :new
            end
        else
            flash[:notice] = "User could not be found"
            render :new
        end
    end
    
    def destroy
        if session[:user_id]
           session[:user_id] = nil 
           redirect_to root_path, notice: 'Successfully signed Out. Come again!'
        end
    end
end
