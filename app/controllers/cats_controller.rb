class CatsController < ApplicationController
    
    def index
        @cats = Cat.all
    end
    
    def show
        @cat = Cat.find(params[:id])
    rescue ActiveRecord::RecordNotFound
        redirect_to cats_path, notice: 'No cats found'
    end
    
    def new
        @cat = Cat.new
    end
    
    def edit
        @cat = Cat.find(params[:id])
    end
    
    def update
       @cat = Cat.find(params[:id]) 
       
       if @cat.update_attributes(valid_cat_params)
          redirect_to cat_url(id: @cat.id), notice: 'Successfully Updated Kitty!' 
       else
           flash[:notice] = "Couldn't update Kitteh :("
           render :edit
           
       end
    end
    
    def create
        @cat = Cat.new(valid_cat_params)
        
        if @cat.save
            redirect_to cats_show_path(id: @cat.id), notice: 'Successfully Created a Cat'
        else
            flash[:notice] = "Failed to Create Cat"
            render :new
        end
    end
    
    def destroy
        @cat = Cat.find(params[:id])
        if @cat.delete
           redirect_to cats_path, notice: 'Deleted Kitteh :`('
        else
            redirect_to request.referrer, "Couldn't destroy kitty"
        end
    end
    
    
    def orange_cats
    end
    
    def valid_cat_params
       params.require(:cat).permit(:name, :colour) 
    end
        
end
