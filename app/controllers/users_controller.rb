class UsersController < ApplicationController
  def index
    @users = User.all
end

  def show
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def create
    @user = User.new(user_params)
    
    if @user.save
      # Send the welcome email
      UserMailer.welcome_email(@user).deliver
      redirect_to user_path(id: @user.id), notice: 'Thanks for Signing Up!'
    else

      render :new
    end
  end
  
  
  def show_by_name
    
    @user = User.where(name: params[:name]).first
    
  end
  
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
